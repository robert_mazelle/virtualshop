package com.robertmazelle.virtualshop.dao;

import com.robertmazelle.virtualshop.entity.Product;
import com.robertmazelle.virtualshop.entity.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.function.Predicate;

@Repository
public class ProductDAO {

    private Map<Integer, Product> products;
    private ProductManager productManager;

    @Autowired
    ProductDAO(ProductManager productManager) {
        products = new HashMap<>();
        this.productManager = productManager;
        initialize();
    }

    ProductDAO() {

    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public Collection<Product> getAllProducts() {
        return this.products.values();
    }

    public int takeNewProduct(Product product) {

        int response = checkValues(product);
        product = reverseValues(product);

        int foundProduct = findProductPrice(product.getName(), product.getPrice());

        if (foundProduct >= 0) {
            int actualQuantity = products.get(foundProduct).getQuantity();
            products.get(foundProduct).setQuantity(actualQuantity + product.getQuantity());
            System.out.println(foundProduct);
            productManager.save(products.get(foundProduct));
        }

        else {
            if (products.get(product.getId()) == null) product.setId(findFreeId());
            products.put(product.getId(), product);
            productManager.save(product);
        }

        return response;
    }

    public boolean putAwayProductById(int id, int quantity) {
        if(getProductById(id) != null) {
            int actualQuantity = products.get(id).getQuantity();
            if (quantity >= actualQuantity) {
                productManager.remove(id);
                products.remove(id);
            } else {
                products.get(id).setQuantity(actualQuantity - quantity);
                productManager.save(products.get(id));
            }
            return true;
        }
        return false;
    }

    public boolean putAwayProductByName(String name, int quantity) {

        Integer productId = findProduct(name);
        int actualQuantity;

        if (productId >= 0) {
            actualQuantity = products.get(productId).getQuantity();

            if (quantity >= actualQuantity) {
                productManager.remove(productId);
                products.remove(productId);
            } else {
                products.get(productId).setQuantity(actualQuantity - quantity);
                productManager.save(products.get(productId));
            }
            return true;
        }
        return false;
    }

    public int updateProduct(Product product) {
        Product findProduct = getProductById(product.getId());
        if (findProduct != null) {
            int response = checkValues(product);
            product = reverseValues(product);
            findProduct.setName(product.getName());
            findProduct.setPrice(product.getPrice());
            findProduct.setQuantity(product.getQuantity());
            products.remove(findProduct.getId());
            products.put(findProduct.getId(),findProduct);
            productManager.save(findProduct);
            return response;
        }
        return 2;
    }

    public double countProductsValue() {
        double value = 0;
        for (Map.Entry<Integer, Product> product : products.entrySet()) {
            value = value + product.getValue().getPrice() * product.getValue().getQuantity();
        }
        value = roundValue(value);
        return value;
    }

    public Integer findProduct(String name) {
        Optional<Integer> foundId = products.entrySet().stream().filter(p -> name.equals(p.getValue().getName())).map(Map.Entry::getKey).findFirst();
        return foundId.orElse(-1);
    }

    public void deleteAllProducts() {
        productManager.clear(products);
        products.clear();
    }

    public double roundValue(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public Product getProductByName(String name) {
        return products.get((findProduct(name)));
    }

    public Product getProductById(int id) {
        return products.get(id);
    }

    public int findFreeId() {
        int i = 0;
        while(products.get(i) != null) i++;
        return i;
    }

    public void initialize() {
        List<Product> productList = this.productManager.initialize();
        for (Product p : productList) {
            products.put(p.getId(), p);
        }
    }

    public int checkValues(Product product) {
        if(product.getPrice() < 0 || product.getQuantity() < 0) return 1;
        return 0;
    }

    public Product reverseValues(Product product) {

        if (product.getPrice() < 0) product.setPrice(-product.getPrice());
        if (product.getQuantity() < 1) product.setQuantity(-product.getQuantity());

        return product;
    }

    public Integer findProductPrice(String name, double price) {
        Predicate<Map.Entry<Integer,Product>> foundName = p -> name.equals(p.getValue().getName());
        Predicate<Map.Entry<Integer,Product>> foundPrice = p -> price == p.getValue().getPrice();
        Optional<Integer> foundProduct = products.entrySet().stream().filter(foundName.and(foundPrice)).map(Map.Entry::getKey).findFirst();
        return foundProduct.orElse(-1);
    }

}
