package com.robertmazelle.virtualshop.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Component
public class ProductManager {

    private Session session;

    public List<Product> initialize() {
        Configuration config = new Configuration().configure().addAnnotatedClass(Product.class);
        SessionFactory sf = config.buildSessionFactory();
        session = sf.openSession();
        Query query = session.createQuery("SELECT p FROM Product p");
        return query.getResultList();
    }

    public void save(Product product) {
        Product findProduct = find(product.getId());
        session.beginTransaction();
        if(findProduct != null) session.merge(product);
        else session.save(product);
        session.getTransaction().commit();
    }

    public void remove(int productId) {
        session.beginTransaction();
        session.remove(find(productId));
        session.getTransaction().commit();
    }

    public void clear(Map<Integer, Product> products) {
        session.beginTransaction();
        for(Map.Entry<Integer, Product> product : products.entrySet()) {
            session.remove(find(product.getValue().getId()));
        }
        session.getTransaction().commit();
    }

    public Product find(int productId) {
        return session.find(Product.class, productId);
    }



}
