package com.robertmazelle.virtualshop.controller;

import com.robertmazelle.virtualshop.service.ShoppingCart;
import com.robertmazelle.virtualshop.entity.Product;
import com.robertmazelle.virtualshop.status.*;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/cart")
public class ShoppingCartController {

    private ShoppingCart shoppingCart;

    @Autowired
    ShoppingCartController(ShoppingCart shoppingCart) { this.shoppingCart = shoppingCart;
    }

    @ApiOperation(value = "Listing all the products in the shopping cart", nickname = "Listing all the products in the shopping cart")
    @GetMapping
    public Collection<Product> getAllProducts() {
        return shoppingCart.getAllProducts();
    }

    @ApiOperation(value = "Adding product to the shopping cart", nickname = "Adding product to the shopping cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product successfully added."),
            @ApiResponse(code = 200, message = "Product successfully added. Negative numbers were reversed.")})
    @PostMapping
    public String takeNewProduct(@ApiParam(name = "Product", value = "\"The parameters of the inserted product. ID isn't necessary - the API can find a free ID. If you want a specific ID, you can try to insert it.", required = true, type = "List<Product>") @RequestBody Product product) {
        int result = shoppingCart.takeNewProduct(product);
        if (result == 0) throw new AddedSuccessfully();
        if (result == 1) throw new AddedSuccessfullyBadSign();
        else throw new UnknownException();
    }

    @ApiOperation(value = "Deleting many products by id or name", nickname = "Deleting many products by id or name")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delete", value = "ID or name of the product", required = true, dataType = "String", example = "Milk"),
            @ApiImplicitParam(name = "quantity", value = "Amount of products to delete", required = true, dataType = "int", example = "1")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All products successfully deleted."),
            @ApiResponse(code = 500, message = "No product with this ID in the database.")})
    @DeleteMapping(value = "{delete}/{quantity}")
    public ResponseEntity putAwayProducts(@PathVariable String delete, @PathVariable int quantity) {
        boolean result = shoppingCart.putAwayProduct(delete, quantity);
        if (result) throw new DeleteManySuccessful();
        else throw new NoIDException();
    }

    @ApiOperation(value = "Delete product by id or name", nickname = "Delete product by id or name")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "delete", value = "ID or name of the product", required = true, dataType = "String", example = "Milk")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product successfully deleted."),
            @ApiResponse(code = 500, message = "No product with this ID in the database.")})
    @DeleteMapping(value = "{delete}")
    public ResponseEntity putAwayProduct(@PathVariable String delete) {
        boolean result = shoppingCart.putAwayProduct(delete, 0);
        if (result) throw new DeleteSuccessful();
        else throw new NoIDException();
    }

    @ApiOperation(value = "Update product", nickname = "Update product")
    @PutMapping
    public ResponseEntity updateProduct(@ApiParam(name = "Product", value = "The parameters of the updated product. The ID parameter is crucial - you are updating the product with this ID", required = true) @RequestBody Product product) {
        int result = shoppingCart.updateProduct(product);
        if (result == 0) throw new UpdateSuccessful();
        else if (result == 1) throw new UpdateSuccessfulBadSign();
        else if (result == 2) throw new NoIDException();
        else throw new UnknownException();
    }


    @ApiOperation(value = "Go to the checkout, deleting all the products from the basket and showing the price to pay", nickname = "Checkout")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "You need to pay **.**$")})
    @GetMapping(value = "checkout")
    public String checkoutShoppingCart() {
        return "You need to pay " + shoppingCart.checkout() + "$";
    }


    @ApiOperation(value = "Getting the required product", nickname = "Getting the required product")
    @GetMapping(value = "{get}")
    public Product getProduct(@ApiParam(name = "ID/name", value = "ID or name of the searched product") @PathVariable String get) {
        return shoppingCart.getProduct(get);
    }


    @ApiOperation(value = "Counting the shopping carts value", nickname = "Counting the shopping carts value")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Your shopping cart's value is: **.**$")})
    @GetMapping(value ="value")
    public String countCartValue(){
        return "Your shopping cart's value is: " + shoppingCart.countProductsValue() + "$";
    }


    @ApiOperation(value = "Adding multiple products to the shopping cart", nickname = "Adding multiple products to the shopping cart")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All products successfully added."),
            @ApiResponse(code = 200, message = "All products successfully added. Every provided negative number was reversed"),})
    @PostMapping(value = "addMany")
    public ResponseEntity takeMoreNewProducts(@ApiParam(name = "List of Products", value = "List of parameters of the inserted products") @RequestBody List<Product> products) {
        int result = shoppingCart.takeMoreNewProducts(products);
        if (result == 0) throw new AddedMany();
        else if (result == 1) throw new AddedManyBadSign();
        else throw new UnknownException();
    }


}
