package com.robertmazelle.virtualshop.service;

import com.robertmazelle.virtualshop.dao.ProductDAO;
import com.robertmazelle.virtualshop.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Service
public class ShoppingCart {

    private ProductDAO productDAO;

    @Autowired
    ShoppingCart(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    public Collection<Product> getAllProducts() {
        return this.productDAO.getAllProducts();
    }

    public int takeNewProduct(Product product) {
        return this.productDAO.takeNewProduct(product);
    }

    public boolean putAwayProduct(String delete, int quantity) {
        int index = 0;
        boolean successfulConversion = true;
        try {
            index = Integer.parseInt(delete);
        } catch (NumberFormatException nfe) {
            successfulConversion = false;
        }
        if (successfulConversion && quantity == 0) return this.productDAO.putAwayProductById(index, 0);
        else if (successfulConversion) return this.productDAO.putAwayProductById(index, quantity);
        else if (quantity == 0) return this.productDAO.putAwayProductByName(delete, 0);
        else return this.productDAO.putAwayProductByName(delete, quantity);
    }

    public int updateProduct(Product product) {
        return this.productDAO.updateProduct(product);
    }

    public double checkout() {
        double checkoutValue = this.productDAO.countProductsValue();
        this.productDAO.deleteAllProducts();
        return checkoutValue;
    }

    public Product getProduct(String get) {
        int index = 0;
        boolean successfulConversion = true;
        try {
            index = Integer.parseInt(get);
        } catch (NumberFormatException nfe) {
            successfulConversion = false;
        }
        if (successfulConversion) return this.productDAO.getProductById(index);
        return this.productDAO.getProductByName(get);
    }

    public double countProductsValue() {
        return this.productDAO.countProductsValue();
    }

    public int takeMoreNewProducts(List<Product> products) {
        int result = 0;
        for (Product p : products)
            if (this.productDAO.takeNewProduct(p) != 0) result = 1;
        return result;
    }

}
