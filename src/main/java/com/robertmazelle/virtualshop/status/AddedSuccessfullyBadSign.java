package com.robertmazelle.virtualshop.status;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.OK, reason="Product successfully added. Negative numbers were reversed.")
public class AddedSuccessfullyBadSign extends RuntimeException {}
