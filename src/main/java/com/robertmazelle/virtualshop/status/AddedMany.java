package com.robertmazelle.virtualshop.status;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.OK, reason="All products successfully added.")
public class AddedMany extends RuntimeException {
}
