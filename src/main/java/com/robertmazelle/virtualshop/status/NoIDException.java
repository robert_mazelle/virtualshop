package com.robertmazelle.virtualshop.status;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="No product with this ID in the database.")
public class NoIDException extends RuntimeException {}