package com.robertmazelle.virtualshop.dao;

import com.robertmazelle.virtualshop.entity.Product;
import org.junit.jupiter.api.*;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class ProductDAOTest {

    ProductDAO productDAO;

    @BeforeEach
    void initialize() {
        productDAO = new ProductDAO();
        Product p0 = new Product(0, "Eggs", 0.51, 10);
        Product p1 = new Product(1, "Milk", 2.12, 2);
        Product p2 = new Product(2, "Cola", 3.71, 1);
        Product p3 = new Product(3, "Butter", 0.96, 2);
        Product p4 = new Product(4, "Water", 0.46, 7);
        Map<Integer, Product> productMap = new HashMap<Integer, Product>();
        productMap.put(0, p0);
        productMap.put(1, p1);
        productMap.put(2, p2);
        productMap.put(3, p3);
        productMap.put(4, p4);
        productDAO.setProducts(productMap);
    }

    @Test
    @Disabled
    public void alwaysFalse() {
        fail("should be fail");
    }

    @Test
    @DisplayName(("Checking the success of the shopping cart counting method"))
    public void countingTestSuccess() {
        double actual = productDAO.countProductsValue();
        double expected = 18.19;
        assertEquals(expected, actual, "Should be " + expected + ", it's " + actual + " instead.");
    }

    @Test
    @DisplayName("Checking the failure of the shopping cart counting method")
    public void countingTestFail() {
        double actual = productDAO.countProductsValue();
        double expected = -4;
        assertNotEquals(expected, actual, "Shouldn't be equal to: " + expected + ", but it is.");
    }

    @Test
    @DisplayName("Checking the success of finding the product's ID")
    public void findProductSuccess() {
       int actual = productDAO.findProduct("Cola");
       int expected = 2;
       assertEquals(expected, actual, "Should be equal to: " + expected + ", but it's " + actual + " instead.");
    }

    @Test
    @DisplayName("Checking the failure of finding the product's ID")
    public void findProductFailure() {
        int actual = productDAO.findProduct("Cola");
        int expected = 4;
        assertNotEquals(expected, actual, "Shouldn't be equal to: " + expected + ", but it is.");
    }

    @Test
    @DisplayName("Checking the success of rounding a number to two decimal places")
    public void roundValueSuccess() {
        double expected = 2.77;
        double actual = productDAO.roundValue(2.7678123);
        assertEquals(expected, actual, "Should be equal to: " + expected + ", but it's " + actual + " instead.");
    }

    @Test
    @DisplayName("Checking the failure of rounding a number to two decimal places")
    public void roundValueFailure() {
        double expected = 2.88;
        double actual = productDAO.roundValue(2.7678123);
        assertNotEquals(expected, actual,"Shouldn't be equal to: " + expected + ", but it is.");
    }

    @Test
    @DisplayName("Checking the success of finding a free ID")
    public void findIDSuccess() {
        int expected = 5;
        int actual = productDAO.findFreeId();
        assertEquals(expected,actual,"Should be equal to: " + expected + ", but it's " + actual + " instead.");
    }

    @Test
    @DisplayName("Checking the failure of finding a free ID")
    public void findIDFailure() {
        int expected = 3;
        int actual = productDAO.findFreeId();
        assertNotEquals(expected,actual,"Shouldn't be equal to: " + expected + ", but it is.");
    }
}